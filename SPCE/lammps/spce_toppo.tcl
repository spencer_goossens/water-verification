#!/usr/bin/tclsh

package require topotools

mol new ../SPCE_804_molecules.pdb autobonds no waitfor all

mol bondsrecalc top

topo retypebonds

topo bondtypenames

topo guessangles

topo guessdihedrals

mol reanalyze top

topo writelammpsdata Lammps_SPCE_data.imp full

quit
