#!/usr/bin/tclsh

package require topotools

mol new ./OPC_804_molecules.pdb autobonds no waitfor all

mol bondsrecalc top

topo retypebonds

topo bondtypenames

topo guessangles

topo guessdihedrals

mol reanalyze top

topo writelammpsdata Lammps_OPC_data.imp full

quit
